Source: r-cran-erm
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-erm
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-erm.git
Homepage: https://cran.r-project.org/package=eRm
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-mass,
               r-cran-matrix,
               r-cran-lattice,
               r-cran-colorspace,
               r-cran-psych
Testsuite: autopkgtest-pkg-r

Package: r-cran-erm
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R package for 'extended Rasch modelling'
 eRm fits Rasch models (RM), linear logistic test models (LLTM),
 rating scale model (RSM), linear rating scale models (LRSM), partial
 credit models (PCM), and linear partial credit models (LPCM). Missing
 values are allowed in the data matrix. Additional features are the ML
 estimation of the person parameters, Andersen's LR-test,
 item-specific Wald test, itemfit and personfit statistics including
 infit and outfit measures, various ICC and related plots, automated
 stepwise item elimination, simulation module for various binary data
 matrices. An eRm platform is provided at R-forge (see URL).
